# Flutter ToastWidgetDD
Toast widget for minimal notification.

## Installing

Add this to your package's `pubspec.yaml` file:

```yaml
dependencies:
  toast_widget_dd: ^latest
```

## Import

```dart
import 'package:toast_widget_dd/toast_widget_dd.dart';
```

## How to use

First, initialize `ToastWidgetDD` in your file`:

Then, enjoy yourself:

```dart
Toast(context).show();
```

## Todo

- [x] add progress indicator

- [x] add custom animation

## Changelog

[CHANGELOG](./CHANGELOG.md)

## License

[MIT License](./LICENSE)
