import 'package:flutter/material.dart';
import 'package:toast_widget_dd/src/toast_page.dart';

class Toast {
  Toast(this.context, {this.duration});
  BuildContext context;
  Duration? duration;

  OverlayEntry? _entry;

  show({String message = 'show toast'}) {
    final overlay = Overlay.of(context);
    _entry = OverlayEntry(
      builder: (context) {
        return ToastPage(
          message: message,
        );
      },
    );
    overlay.insert(_entry!);
    Future.delayed(duration ?? const Duration(seconds: 2))
        .then((value) {})
        .then((value) {
      _entry!.remove();
      _entry == null;
    });
  }
}
